# Packard Interactive Light Sculpture Project

This repository stores all of the files, notes, and technical documents
for the Packard Interactive Light Sculpture Project. Stanford students
taking EE185, EE285, and CS241, under guidance from course instructors,
engineer an art piece designed by Charles Gadeken. This engineering involves
designing, building, and testing the mechanical, electrical, and software
components of the pieces.

The directory structure:
  - autumn: materials from the Autumn offering of EE185
  - cad: CAD drawings and designs for the piece
  - datasheets: technical documentation of parts the piece uses
  - design-docs: documentation of design decisions in the piece
  - projects: writeups of the 1-3 week projects students are tackling to engineer the piece

The Drawing Tree of the FRACTAL\_FLYER is in this google doc:
https://docs.google.com/document/d/1t9YB293jo6p6bc5lDQ5S1L3p2o_mtnMwk6ygtS_yizA/edit
The drawing tree helps us codify our shared vision of the final product.
It is a living document; please modify and elaborate as we solidify our designs.
