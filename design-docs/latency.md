# Cable Latency Report

TODO:

* [ ] Do a functional test of a 200 ft (100 ft \* 2) cable length
* [ ] Find length which works for SAM32 programming.
* [x] Confirm that the USB Mouse works over 200 ft cable (a low-speed device)
* [x] Create a generic female USB A connector for testing.

## Debugging USB

We can use `dmesg` to monitor the USB devices connected and disconnected. For
example, to view whether a device connects and high/full/low speed.

```
$ dmesg -L -w
```

## Measurements

As expected, the 100 ft Cat7 cable accounts for about 150 ns of delay. We were
initially concerned about this latency because [1] suggests that we only have on
the order of 380ns of delay allowed in either direction.

![100-ft-cable-delay](data/100-ft-cable-delay.png)

However, we also find that we we look at the one-way delay of the setup as a
whole, we see see about 80 us between a packet sent by the device (which appears
after the left cursor), and the packet appearing on the cable to the host after
the USB hub (after the right cursor).

![one-way](data/one-way-delay.png)

We also see about 80 us delay if we use
[Wireshark](https://wiki.wireshark.org/CaptureSetup/USB) to capture the USB
packets and look at the difference in timestamps of the packets. An [example USB
trace](data/usb-trace.pcap) is provided.

# References

* [1]: [An Introduction to USB Extenders](https://www.ieci.com.au/applications/wp-usb-extender.pdf)
* [2]: [USB 2.0 Specification](http://sdphca.ucsd.edu/lab_equip_manuals/usb_20.pdf)
