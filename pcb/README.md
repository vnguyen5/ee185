# PCB Design

The hardware for the fractal flyers consists mainly of the head board and tail
board. The design files for these boards are located in this directory.

# Reference

* [KiCad](https://kicad-pcb.org/)
* [SAM32 Board Files](https://github.com/maholli/SAM32/tree/master/hardware/SAM32_v26)
* [TI WEBENCH Power Designer](https://www.ti.com/design-resources/design-tools-simulation/webench-power-designer.html)
* [Upverter](https://upverter.com/)
