EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 21832 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1900 4050 1900 7000
Wire Wire Line
	5900 5400 5900 7000
Wire Wire Line
	12400 5050 12500 5050
Wire Wire Line
	12400 4750 12500 4750
Wire Wire Line
	12400 4550 12500 4550
Wire Wire Line
	12500 7000 12500 4550
Wire Wire Line
	12500 4950 12400 4950
Wire Wire Line
	1900 4050 2600 4050
Wire Wire Line
	5900 5400 6600 5400
Wire Wire Line
	1900 3450 1900 1100
Wire Wire Line
	1900 3450 2600 3450
Wire Wire Line
	10450 2550 10450 2000
Wire Wire Line
	10450 2000 9450 2000
Wire Wire Line
	10600 2550 10450 2550
Wire Wire Line
	13100 5000 13100 4050
Wire Wire Line
	13100 5000 17300 5000
Wire Wire Line
	16050 6000 16950 6000
Wire Wire Line
	16050 5500 16050 6450
Wire Wire Line
	16950 6000 16950 5000
Wire Wire Line
	17300 4650 18300 4650
Wire Wire Line
	10600 2950 4150 2950
Wire Wire Line
	10600 3350 5000 3350
Wire Wire Line
	5900 4800 5900 3650
Wire Wire Line
	10600 3650 5900 3650
Wire Wire Line
	5900 4800 6600 4800
Wire Wire Line
	12800 6000 13800 6000
Wire Wire Line
	12800 4350 12800 6000
Wire Wire Line
	13800 5500 13800 6450
Wire Wire Line
	13350 3850 13350 3550
Wire Wire Line
	9400 4250 10600 4250
Wire Wire Line
	12650 2600 12650 2750
Wire Wire Line
	12400 2550 12400 2250
Wire Wire Line
	13700 2600 13700 3050
$Comp
L Device:C C?
U 1 1 2778CB69
P 19150 4750
F 0 "C?" H 19000 4650 66  0000 R TNN
F 1 "10uF" H 19000 4800 66  0000 R TNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 19150 4750 50  0001 C CNN
F 3 "" H 19150 4750 50  0001 C CNN
F 4 "CL32A106KAULNNE" H 19150 4750 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 19150 4750 50  0001 C CNN "Manufacturer"
F 6 "1276-1854-1-ND" H 19150 4750 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 10UF 25V X5R 1210" H 19150 4750 50  0001 C CNN "Description"
	1    19150 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 C6152A4C
P 9450 1600
F 0 "R?" H 9350 1550 66  0000 R TNN
F 1 "2.1" H 9350 1700 66  0000 R TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9450 1600 50  0001 C CNN
F 3 "" H 9450 1600 50  0001 C CNN
F 4 "CRCW06032R10FKEA" H 9450 1600 50  0001 C CNN "Manufacturer PN"
F 5 "Vishay Dale" H 9450 1600 50  0001 C CNN "Manufacturer"
F 6 "541-2.10HHCT-ND" H 9450 1600 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 2.1 OHM 1% 1/10W 0603" H 9450 1600 50  0001 C CNN "Description"
	1    9450 1600
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 E936CE3F
P 18300 4250
F 0 "C?" H 18150 4150 66  0000 R TNN
F 1 "470pF" H 18150 4300 66  0000 R TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 18300 4250 50  0001 C CNN
F 3 "" H 18300 4250 50  0001 C CNN
F 4 "CC0603KRX7R9BB471" H 18300 4250 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" H 18300 4250 50  0001 C CNN "Manufacturer"
F 6 "311-1078-1-ND" H 18300 4250 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 470PF 50V X7R 0603" H 18300 4250 50  0001 C CNN "Description"
	1    18300 4250
	-1   0    0    1   
$EndComp
$Comp
L power:GND GND10
U 1 1 2FCAD35E
P 15750 4350
F 0 "GND10" H 15750 4570 50  0001 C BNN
F 1 "GND" H 15750 4350 50  0001 C CNN
F 2 "2020-02-03_08-53-33:WB_GND" H 15750 4350 50  0001 C CNN
F 3 "" H 15750 4350 50  0001 C CNN
	1    15750 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 FB4E1E70
P 4150 2000
F 0 "R?" H 4050 1900 66  0000 R TNN
F 1 "200k" H 4050 2050 66  0000 R TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4150 2000 50  0001 C CNN
F 3 "" H 4150 2000 50  0001 C CNN
F 4 "RC0603FR-07200KL" H 4150 2000 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" H 4150 2000 50  0001 C CNN "Manufacturer"
F 6 "311-200KHRCT-ND" H 4150 2000 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 200K OHM 1% 1/10W 0603" H 4150 2000 50  0001 C CNN "Description"
	1    4150 2000
	-1   0    0    1   
$EndComp
$Comp
L power:GND GND6
U 1 1 D9C96CCE
P 11500 7100
F 0 "GND6" H 11500 7320 50  0001 C BNN
F 1 "GND" H 11500 7100 50  0001 C CNN
F 2 "2020-02-03_08-53-33:WB_GND" H 11500 7100 50  0001 C CNN
F 3 "" H 11500 7100 50  0001 C CNN
	1    11500 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 57C839B6
P 14250 5500
F 0 "C?" V 14500 5250 66  0000 C TNN
F 1 "15nF" V 14350 5300 66  0000 C TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 14250 5500 50  0001 C CNN
F 3 "" H 14250 5500 50  0001 C CNN
F 4 "CL10B153KB8NNNC" V 14250 5500 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" V 14250 5500 50  0001 C CNN "Manufacturer"
F 6 "1276-1277-1-ND" V 14250 5500 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.015UF 50V X7R 0603" V 14250 5500 50  0001 C CNN "Description"
	1    14250 5500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 DB5C0A27
P 4150 4300
F 0 "R?" H 4000 4200 66  0000 R TNN
F 1 "7.68k" H 4000 4350 66  0000 R TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4150 4300 50  0001 C CNN
F 3 "" H 4150 4300 50  0001 C CNN
F 4 "RC0603FR-077K68L" H 4150 4300 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" H 4150 4300 50  0001 C CNN "Manufacturer"
F 6 "311-7.68KHRCT-ND" H 4150 4300 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 7.68K OHM 1% 1/10W 0603" H 4150 4300 50  0001 C CNN "Description"
	1    4150 4300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 EE1463A4
P 17300 3800
F 0 "R?" H 17200 3700 66  0000 R TNN
F 1 "18k" H 17200 3850 66  0000 R TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 17300 3800 50  0001 C CNN
F 3 "" H 17300 3800 50  0001 C CNN
F 4 "RC0603FR-0718KL" H 17300 3800 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" H 17300 3800 50  0001 C CNN "Manufacturer"
F 6 "311-18.0KHRCT-ND" H 17300 3800 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 18K OHM 1% 1/10W 0603" H 17300 3800 50  0001 C CNN "Description"
	1    17300 3800
	-1   0    0    1   
$EndComp
$Comp
L 2020-02-03_08-53-33-eagle-import:CSD19537Q3CSD19537Q3 M?
U 1 1 B5CDC3DC
P 14550 2250
F 0 "M?" H 14850 2500 66  0000 C TNN
F 1 "100V" H 14850 2300 66  0000 C BNN
F 2 "" H 14550 2250 50  0001 C CNN
F 3 "" H 14550 2250 50  0001 C CNN
F 4 "CSD19538Q3A" H 14550 2250 50  0001 C CNN "Manufacturer PN"
F 5 "Texas Instruments" H 14550 2250 50  0001 C CNN "Manufacturer"
F 6 "296-44352-1-ND" H 14550 2250 50  0001 C CNN "Digikey PN"
F 7 "MOSFET N-CH 100V 15A VSONP" H 14550 2250 50  0001 C CNN "Description"
	1    14550 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND GND12
U 1 1 A3015704
P 13650 4800
F 0 "GND12" H 13650 5020 50  0001 C BNN
F 1 "GND" H 13650 4800 50  0001 C CNN
F 2 "2020-02-03_08-53-33:WB_GND" H 13650 4800 50  0001 C CNN
F 3 "" H 13650 4800 50  0001 C CNN
	1    13650 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 DEC8C886
P 9450 2350
F 0 "C?" H 9300 2250 66  0000 R TNN
F 1 "100nF" H 9300 2400 66  0000 R TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9450 2350 50  0001 C CNN
F 3 "" H 9450 2350 50  0001 C CNN
F 4 "CL10B104KC8NNNC" H 9450 2350 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 9450 2350 50  0001 C CNN "Manufacturer"
F 6 "1276-6807-1-ND" H 9450 2350 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.1UF 100V X7R 0603" H 9450 2350 50  0001 C CNN "Description"
	1    9450 2350
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 79593932
P 10000 4550
F 0 "R?" V 9750 4900 66  0000 C TNN
F 1 "20k" V 9900 4850 66  0000 C TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10000 4550 50  0001 C CNN
F 3 "" H 10000 4550 50  0001 C CNN
F 4 "RC0603FR-0720KL" V 10000 4550 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" V 10000 4550 50  0001 C CNN "Manufacturer"
F 6 "311-20.0KHRCT-ND" V 10000 4550 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 20K OHM 1% 1/10W 0603" V 10000 4550 50  0001 C CNN "Description"
	1    10000 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 E1E22563
P 13650 4300
F 0 "C?" H 13800 4400 66  0000 L BNN
F 1 "6.8pF" H 13800 4250 66  0000 L BNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 13650 4300 50  0001 C CNN
F 3 "" H 13650 4300 50  0001 C CNN
F 4 "CL10C6R8DB8NNNC" H 13650 4300 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 13650 4300 50  0001 C CNN "Manufacturer"
F 6 "1276-2334-1-ND" H 13650 4300 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 6.8PF 50V C0G/NP0 0603" H 13650 4300 50  0001 C CNN "Description"
	1    13650 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 CC3AC024
P 14250 3350
F 0 "R?" V 14050 3600 66  0000 C BNN
F 1 "1.18k" V 14200 3600 66  0000 C BNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 14250 3350 50  0001 C CNN
F 3 "" H 14250 3350 50  0001 C CNN
F 4 "RC0603FR-071K18L" V 14250 3350 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" V 14250 3350 50  0001 C CNN "Manufacturer"
F 6 "311-1.18KHRCT-ND" V 14250 3350 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 1.18K OHM 1% 1/10W 0603" V 14250 3350 50  0001 C CNN "Description"
	1    14250 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 3C046C81
P 9400 5700
F 0 "C?" H 9250 5600 66  0000 R TNN
F 1 "2.2uF" H 9250 5750 66  0000 R TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9400 5700 50  0001 C CNN
F 3 "" H 9400 5700 50  0001 C CNN
F 4 "CL10A225KO8NNNC" H 9400 5700 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 9400 5700 50  0001 C CNN "Manufacturer"
F 6 "1276-1040-1-ND" H 9400 5700 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 2.2UF 16V X5R 0603" H 9400 5700 50  0001 C CNN "Description"
	1    9400 5700
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 0F5EE54F
P 1900 3750
F 0 "C?" H 1750 3650 66  0000 R TNN
F 1 "2.2uF" H 1750 3800 66  0000 R TNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 1900 3750 50  0001 C CNN
F 3 "" H 1900 3750 50  0001 C CNN
F 4 "CL32B225KCJSNNE" H 1900 3750 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 1900 3750 50  0001 C CNN "Manufacturer"
F 6 "1276-3362-2-ND" H 1900 3750 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 2.2UF 100V X7R 1210" H 1900 3750 50  0001 C CNN "Description"
	1    1900 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 C8D8F2A6
P 15650 5500
F 0 "R?" V 15900 5300 66  0000 C TNN
F 1 "2.67k" V 15750 5350 66  0000 C TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 15650 5500 50  0001 C CNN
F 3 "" H 15650 5500 50  0001 C CNN
F 4 "RC0603FR-072K67L" V 15650 5500 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" V 15650 5500 50  0001 C CNN "Manufacturer"
F 6 "311-2.67KHRCT-ND" V 15650 5500 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 2.67K OHM 1% 1/10W 0603" V 15650 5500 50  0001 C CNN "Descriptor"
	1    15650 5500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 7D17B0BF
P 17300 5900
F 0 "R?" H 17400 6000 66  0000 L BNN
F 1 "845" H 17400 5850 66  0000 L BNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 17300 5900 50  0001 C CNN
F 3 "" H 17300 5900 50  0001 C CNN
F 4 "RC0603FR-07845RL" V 17300 5900 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" V 17300 5900 50  0001 C CNN "Manufacturer"
F 6 "311-845HRTR-ND" V 17300 5900 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 845 OHM 1% 1/10W 0603" V 17300 5900 50  0001 C CNN "Description"
	1    17300 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND GND11
U 1 1 7A552D41
P 9450 2750
F 0 "GND11" H 9450 2970 50  0001 C BNN
F 1 "GND" H 9450 2750 50  0001 C CNN
F 2 "2020-02-03_08-53-33:WB_GND" H 9450 2750 50  0001 C CNN
F 3 "" H 9450 2750 50  0001 C CNN
	1    9450 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 95ECA3A0
P 18300 3550
F 0 "R?" H 18200 3450 66  0000 R TNN
F 1 "17.4" H 18200 3600 66  0000 R TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 18300 3550 50  0001 C CNN
F 3 "" H 18300 3550 50  0001 C CNN
F 4 "RC0603FR-0717R4L" H 18300 3550 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" H 18300 3550 50  0001 C CNN "Manufacturer"
F 6 "311-17.4HRCT-ND" H 18300 3550 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 17.4 OHM 1% 1/10W 0603" H 18300 3550 50  0001 C CNN "Description"
	1    18300 3550
	-1   0    0    1   
$EndComp
$Comp
L pspice:INDUCTOR L?
U 1 1 6A4EAB05
P 16550 3050
F 0 "L?" H 16250 3300 66  0000 C BNN
F 1 "4.7uH" H 16200 3150 66  0000 C BNN
F 2 "" H 16550 3050 50  0001 C CNN
F 3 "" H 16550 3050 50  0001 C CNN
F 4 "SRP1270-4R7M" H 16550 3050 50  0001 C CNN "Manufacturer PN"
F 5 "Bourns Inc." H 16550 3050 50  0001 C CNN "Manufacturer"
F 6 "SRP1270-4R7MCT-ND" H 16550 3050 50  0001 C CNN "Digikey PN"
F 7 "FIXED IND 4.7UH 15A 11.2MOHM SMD" H 16550 3050 50  0001 C CNN "Description"
	1    16550 3050
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 EECD621B
P 14900 6450
F 0 "C?" V 15150 6200 66  0000 C TNN
F 1 "1pF" V 15000 6250 66  0000 C TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 14900 6450 50  0001 C CNN
F 3 "" H 14900 6450 50  0001 C CNN
F 4 "CL10C010CB8NNNC" V 14900 6450 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" V 14900 6450 50  0001 C CNN "Manufacturer"
F 6 "1276-1293-1-ND" V 14900 6450 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 1PF 50V C0G/NP0 0603" V 14900 6450 50  0001 C CNN "Description"
	1    14900 6450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12400 4050 13100 4050
Wire Wire Line
	12400 3550 13350 3550
Wire Wire Line
	12400 2750 12650 2750
$Comp
L 2020-02-03_08-53-33-eagle-import:LM5145RGYRLM5145RGYR U?
U 1 1 8AECD00E
P 11500 3750
F 0 "U?" H 11050 5200 66  0000 C BNN
F 1 "LM5145RGYR" H 11900 2250 66  0000 C CNN
F 2 "" H 11500 3750 50  0001 C CNN
F 3 "" H 11500 3750 50  0001 C CNN
F 4 "LM5145RGYR" H 11500 3750 50  0001 C CNN "Manufacturer PN"
F 5 "Texas Instruments" H 11500 3750 50  0001 C CNN "Manufacturer"
F 6 "296-47661-1-ND" H 11500 3750 50  0001 C CNN "Digikey PN"
F 7 "Buck Regulator Positive Output Step-Down DC-DC Controller" H 11500 3750 50  0001 C CNN "Description"
	1    11500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	12400 3050 15750 3050
Wire Wire Line
	16800 3050 17300 3050
Wire Wire Line
	19150 7000 17300 7000
Connection ~ 4150 7000
Wire Wire Line
	4150 7000 1900 7000
Connection ~ 5000 7000
Wire Wire Line
	5000 7000 4150 7000
Connection ~ 5900 7000
Wire Wire Line
	5900 7000 5000 7000
Connection ~ 9400 7000
Wire Wire Line
	9400 7000 5900 7000
Connection ~ 12500 7000
Connection ~ 17300 7000
Wire Wire Line
	17300 7000 12500 7000
Connection ~ 17300 3050
Wire Wire Line
	17300 3050 18300 3050
Connection ~ 18300 3050
Wire Wire Line
	18300 3050 19150 3050
Wire Wire Line
	19850 5250 19150 5250
Connection ~ 19150 5250
Wire Wire Line
	19850 4200 19150 4200
Connection ~ 19150 4200
Wire Wire Line
	19150 4200 19150 3050
Connection ~ 4150 1100
Wire Wire Line
	4150 1100 9450 1100
Connection ~ 9450 1100
Wire Wire Line
	9450 1100 14700 1100
Wire Wire Line
	12400 4350 12800 4350
$Comp
L Device:C C?
U 1 1 253BD765
P 5900 5100
F 0 "C?" H 5750 5000 66  0000 R TNN
F 1 "18nF" H 5750 5150 66  0000 R TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5900 5100 50  0001 C CNN
F 3 "" H 5900 5100 50  0001 C CNN
F 4 "LMK107SD183JA-T" H 5900 5100 50  0001 C CNN "Manufacturer PN"
F 5 "Taiyo Yuden" H 5900 5100 50  0001 C CNN "Manufacturer"
F 6 "587-1099-1-ND" H 5900 5100 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.018UF 10V 0603" H 5900 5100 50  0001 C CNN "Description"
	1    5900 5100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 21CB5960
P 5000 5100
F 0 "R?" H 4850 5000 66  0000 R TNN
F 1 "23.2k" H 4850 5150 66  0000 R TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5000 5100 50  0001 C CNN
F 3 "" H 5000 5100 50  0001 C CNN
F 4 "RC0603FR-0723K2L" H 5000 5100 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" H 5000 5100 50  0001 C CNN "Manufacturer"
F 6 "311-23.2KHRCT-ND" H 5000 5100 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 23.2K OHM 1% 1/10W 0603" H 5000 5100 50  0001 C CNN "Description"
	1    5000 5100
	-1   0    0    1   
$EndComp
Wire Wire Line
	19150 5250 19150 7000
Wire Wire Line
	19150 4900 19150 5250
Wire Wire Line
	19850 4900 19850 5250
Wire Wire Line
	19150 4200 19150 4600
Wire Wire Line
	19850 4200 19850 4600
Wire Wire Line
	17300 3050 17300 3650
Wire Wire Line
	18300 3050 18300 3400
Wire Wire Line
	18300 3700 18300 4100
Wire Wire Line
	18300 4400 18300 4650
Wire Wire Line
	17300 6050 17300 7000
Wire Wire Line
	17300 3950 17300 5750
Wire Wire Line
	15050 6450 16050 6450
Wire Wire Line
	13800 6450 14750 6450
Wire Wire Line
	13800 5500 14100 5500
Wire Wire Line
	14400 5500 15500 5500
Wire Wire Line
	15800 5500 16050 5500
Wire Wire Line
	9400 3950 9400 5550
Wire Wire Line
	9400 5850 9400 7000
Wire Wire Line
	9400 4550 9850 4550
Wire Wire Line
	10150 4550 10600 4550
Wire Wire Line
	9400 3950 9950 3950
Wire Wire Line
	10250 3950 10600 3950
Wire Wire Line
	7300 4800 7300 4950
Wire Wire Line
	7300 5250 7300 5400
Wire Wire Line
	6600 5250 6600 5400
Connection ~ 6600 5400
Wire Wire Line
	6600 5400 7300 5400
Wire Wire Line
	6600 4950 6600 4800
Connection ~ 6600 4800
Wire Wire Line
	6600 4800 7300 4800
Wire Wire Line
	5900 4800 5900 4950
Connection ~ 5900 4800
Wire Wire Line
	5900 5250 5900 5400
Connection ~ 5900 5400
Wire Wire Line
	5000 5250 5000 7000
Wire Wire Line
	5000 3350 5000 4950
Wire Wire Line
	4150 4450 4150 7000
Wire Wire Line
	1900 3600 1900 3450
Connection ~ 1900 3450
Wire Wire Line
	1900 3900 1900 4050
Connection ~ 1900 4050
Wire Wire Line
	2600 3900 2600 4050
Connection ~ 2600 4050
Wire Wire Line
	2600 4050 3300 4050
Wire Wire Line
	2600 3600 2600 3450
Connection ~ 2600 3450
Wire Wire Line
	2600 3450 3300 3450
Wire Wire Line
	3300 3600 3300 3450
Wire Wire Line
	3300 3900 3300 4050
Wire Wire Line
	4150 1100 4150 1850
Wire Wire Line
	4150 2150 4150 4150
Wire Wire Line
	9450 2500 9450 2750
Wire Wire Line
	9450 1750 9450 2200
Wire Wire Line
	9450 1100 9450 1450
Wire Wire Line
	12400 3350 14100 3350
Wire Wire Line
	14400 3350 15750 3350
Wire Wire Line
	13650 3350 13650 4150
Wire Wire Line
	13300 2600 13700 2600
Wire Wire Line
	12650 2600 13000 2600
Wire Wire Line
	9400 7000 11500 7000
Wire Wire Line
	13650 4450 13650 4800
Wire Wire Line
	11500 7100 11500 7000
Connection ~ 11500 7000
Wire Wire Line
	11500 7000 12500 7000
Wire Wire Line
	13350 3850 15350 3850
Wire Wire Line
	15750 3450 15750 3350
Wire Wire Line
	15750 4250 15750 4350
Connection ~ 1900 1100
Wire Wire Line
	1900 1100 4150 1100
Wire Wire Line
	1900 950  1900 1100
$Comp
L power:+48V #PWR?
U 1 1 5E57FB9D
P 1900 950
F 0 "#PWR?" H 1900 800 50  0001 C CNN
F 1 "+48V" H 1915 1123 50  0000 C CNN
F 2 "" H 1900 950 50  0001 C CNN
F 3 "" H 1900 950 50  0001 C CNN
	1    1900 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+15V #PWR?
U 1 1 5E58ADCD
P 19150 2900
F 0 "#PWR?" H 19150 2750 50  0001 C CNN
F 1 "+15V" H 19165 3073 50  0000 C CNN
F 2 "" H 19150 2900 50  0001 C CNN
F 3 "" H 19150 2900 50  0001 C CNN
	1    19150 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	19150 2900 19150 3050
Connection ~ 19150 3050
$Comp
L Device:C C?
U 1 1 5E5907E6
P 2600 3750
F 0 "C?" H 2450 3650 66  0000 R TNN
F 1 "2.2uF" H 2450 3800 66  0000 R TNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 2600 3750 50  0001 C CNN
F 3 "" H 2600 3750 50  0001 C CNN
F 4 "CL32B225KCJSNNE" H 2600 3750 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 2600 3750 50  0001 C CNN "Manufacturer"
F 6 "1276-3362-2-ND" H 2600 3750 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 2.2UF 100V X7R 1210" H 2600 3750 50  0001 C CNN "Description"
	1    2600 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5E591006
P 3300 3750
F 0 "C?" H 3150 3650 66  0000 R TNN
F 1 "2.2uF" H 3150 3800 66  0000 R TNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 3300 3750 50  0001 C CNN
F 3 "" H 3300 3750 50  0001 C CNN
F 4 "CL32B225KCJSNNE" H 3300 3750 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 3300 3750 50  0001 C CNN "Manufacturer"
F 6 "1276-3362-2-ND" H 3300 3750 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 2.2UF 100V X7R 1210" H 3300 3750 50  0001 C CNN "Description"
	1    3300 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5E5920EA
P 6600 5100
F 0 "C?" H 6450 5000 66  0000 R TNN
F 1 "18nF" H 6450 5150 66  0000 R TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6600 5100 50  0001 C CNN
F 3 "" H 6600 5100 50  0001 C CNN
F 4 "LMK107SD183JA-T" H 6600 5100 50  0001 C CNN "Manufacturer PN"
F 5 "Taiyo Yuden" H 6600 5100 50  0001 C CNN "Manufacturer"
F 6 "587-1099-1-ND" H 6600 5100 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.018UF 10V 0603" H 6600 5100 50  0001 C CNN "Description"
	1    6600 5100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5E592B14
P 7300 5100
F 0 "C?" H 7150 5000 66  0000 R TNN
F 1 "18nF" H 7150 5150 66  0000 R TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7300 5100 50  0001 C CNN
F 3 "" H 7300 5100 50  0001 C CNN
F 4 "LMK107SD183JA-T" H 7300 5100 50  0001 C CNN "Manufacturer PN"
F 5 "Taiyo Yuden" H 7300 5100 50  0001 C CNN "Manufacturer"
F 6 "587-1099-1-ND" H 7300 5100 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.018UF 10V 0603" H 7300 5100 50  0001 C CNN "Description"
	1    7300 5100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5E593ACE
P 13150 2600
F 0 "C?" V 12850 2850 66  0000 R TNN
F 1 "100nF" V 13000 3000 66  0000 R TNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 13150 2600 50  0001 C CNN
F 3 "" H 13150 2600 50  0001 C CNN
F 4 "CL10B104KC8NNNC" H 13150 2600 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 13150 2600 50  0001 C CNN "Manufacturer"
F 6 "1276-6807-1-ND" H 13150 2600 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.1UF 100V X7R 0603" H 13150 2600 50  0001 C CNN "Description"
	1    13150 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	15750 3350 15750 3050
Connection ~ 15750 3350
Connection ~ 15750 3050
Wire Wire Line
	15750 3050 16300 3050
NoConn ~ 10600 4750
NoConn ~ 10600 4950
NoConn ~ 10600 5050
$Comp
L Device:C C?
U 1 1 5E5B5683
P 19850 4750
F 0 "C?" H 19700 4650 66  0000 R TNN
F 1 "10uF" H 19700 4800 66  0000 R TNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 19850 4750 50  0001 C CNN
F 3 "" H 19850 4750 50  0001 C CNN
F 4 "CL32A106KAULNNE" H 19850 4750 50  0001 C CNN "Manufacturer PN"
F 5 "Samsung Electro-Mechanics" H 19850 4750 50  0001 C CNN "Manufacturer"
F 6 "1276-1854-1-ND" H 19850 4750 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 10UF 25V X5R 1210" H 19850 4750 50  0001 C CNN "Description"
	1    19850 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5E3B940E
P 10100 3950
F 0 "R?" V 9850 4300 66  0000 C TNN
F 1 "20k" V 10000 4250 66  0000 C TNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10100 3950 50  0001 C CNN
F 3 "" H 10100 3950 50  0001 C CNN
F 4 "RC0603FR-0720KL" V 10100 3950 50  0001 C CNN "Manufacturer PN"
F 5 "Yageo" V 10100 3950 50  0001 C CNN "Manufacturer"
F 6 "311-20.0KHRCT-ND" V 10100 3950 50  0001 C CNN "Digikey PN"
F 7 "RES SMD 20K OHM 1% 1/10W 0603" V 10100 3950 50  0001 C CNN "Description"
	1    10100 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	12400 2250 14300 2250
Wire Wire Line
	14700 2550 14700 3050
Wire Wire Line
	14700 1100 14700 1850
$Comp
L 2020-02-03_08-53-33-eagle-import:CSD19537Q3CSD19537Q3 M?
U 1 1 5E3BC627
P 15600 3850
F 0 "M?" H 15900 4100 66  0000 C TNN
F 1 "100V" H 15900 3900 66  0000 C BNN
F 2 "" H 15600 3850 50  0001 C CNN
F 3 "" H 15600 3850 50  0001 C CNN
F 4 "CSD19538Q3A" H 15600 3850 50  0001 C CNN "Manufacturer PN"
F 5 "Texas Instruments" H 15600 3850 50  0001 C CNN "Manufacturer"
F 6 "296-44352-1-ND" H 15600 3850 50  0001 C CNN "Digikey PN"
F 7 "MOSFET N-CH 100V 15A VSONP" H 15600 3850 50  0001 C CNN "Description"
	1    15600 3850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
